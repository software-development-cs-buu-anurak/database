/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.anurak.databaseproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author Sarocha
 */
public class UpdateDatabase {

    public static void main(String[] args) {
        Connection conn = null;
        String url = "jdbc:sqlite:dcoffee.db";

        //Connection database
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connection to SQLite has been estabish.");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return;
        }

        //Update
        String sql = "UPDATE CATEGORY SET CATEGORY_NAME=? WHERE CATEGORY_ID=?";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, "Cake");
            stmt.setString(2, "CTG004");
            int satatus = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        //Close database
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }

    }
}

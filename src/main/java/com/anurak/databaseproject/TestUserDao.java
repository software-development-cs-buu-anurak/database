/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.anurak.databaseproject;

import com.anurak.databaseproject.dao.UserDao;
import com.anurak.databaseproject.model.User;

import helper.DatabaseHelper;

/**
 *
 * @author Sarocha
 */
public class TestUserDao {
    public static void main(String[] args) {
//        System.out.println("Hello");
        UserDao userDao = new UserDao();
        
        for(User u : userDao.getAll()) {
           System.out.println(u);
        }
//        System.out.println();
//        User user1 = userDao.get(1);
//        System.out.println(user1);
        
//        User newUser = new User("user3", "password", 2, "M");        
//        User insertedUser = userDao.save(newUser);
//        System.out.println(insertedUser);

//        user1.setGender("F");
//        userDao.update(user1);
//        User updateUser = userDao.get(1);
//        System.out.println(updateUser);
//        
//        userDao.delete(user1);
//        for(User u : userDao.getAll()) {
//           System.out.println(u);
//        }
//        
//        for(User u : userDao.getAll(" USER_NAME like 'U%' ", " USER_NAME asc, USER_GENDER desc ")) {
//           System.out.println(u);
//        }
//        
        DatabaseHelper.close();
    }
}


/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.anurak.databaseproject;

import com.anurak.databaseproject.model.User;
import com.anurak.databaseproject.service.UserService;

/**
 *
 * @author Sarocha
 */
public class TestUserService {
    public static void main(String[] args) {
        UserService userService = new UserService();
        User user = userService.login("user1", "password");
        if (user != null) {
           System.out.println("Welcome user : " + user.getName());
        }else {
           System.out.println("error");
        }
    }
}
